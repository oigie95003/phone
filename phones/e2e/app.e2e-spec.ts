import { PhonesPage } from './app.po';

describe('phones App', function() {
  let page: PhonesPage;

  beforeEach(() => {
    page = new PhonesPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
