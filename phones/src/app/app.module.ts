import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { PhoneComponent } from './phone/phone.component';
import { ShoppingListComponent } from './shopping-list/shopping-list.component';
import { ShoppingEditComponent } from './shopping-list/shopping-edit/shopping-edit.component';
import { PhoneDetailComponent } from './phone/phone-detail/phone-detail.component';
import { PhoneListComponent } from './phone/phone-list/phone-list.component';
import { PhoneItemComponent } from './phone/phone-list/phone-item/phone-item.component';
import { HeaderComponent } from './header/header.component';
import { dropdownDirective } from './shared/dropdown.directive';
import { ShoppingListService } from './shopping-list/shopping-list.service';

@NgModule({
  declarations: [
    AppComponent,
    PhoneComponent,
    ShoppingListComponent,
    ShoppingEditComponent,
    PhoneDetailComponent,
    PhoneListComponent,
    PhoneItemComponent,
    HeaderComponent,
    dropdownDirective,
    
   
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
  
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
