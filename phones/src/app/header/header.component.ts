import { Component, OnInit, Output } from '@angular/core';
import { EventEmitter } from '@angular/common/src/facade/async';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
   @Output() wellSelected = new EventEmitter<string>();

  onSelect(well:string){
      this.wellSelected.emit(well);
  }
  constructor() { }

  ngOnInit() {
  }

}
