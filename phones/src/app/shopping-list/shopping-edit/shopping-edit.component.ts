import { Component, OnInit, ElementRef, ViewChild,  Output } from '@angular/core';
import { Accesseries } from '../../shared/accesseries.model';
import { ShoppingListService } from '../shopping-list.service';


@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit {
   @ViewChild('nameInput') nameInputRef: ElementRef ; 
   @ViewChild('amountInput') amountInputRef: ElementRef;
   //@Output() accesseriesAdded = new EventEmitter<Accesseries>();
  constructor(private slService: ShoppingListService) { }

  ngOnInit() {
  }

onAddItem(){
  const ingName = this.nameInputRef.nativeElement.value;
  const ingAmount =  this.amountInputRef.nativeElement.value;
  const newAccesseries = new Accesseries(ingName, ingAmount);
  //this.accesseriesAdded.emit(newAccesseries);
  this.slService.addAccesseries(newAccesseries);
 }
}
