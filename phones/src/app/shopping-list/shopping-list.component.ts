import { Component, OnInit } from '@angular/core';
import { Accesseries } from '../shared/accesseries.model';
import { ShoppingListService } from './shopping-list.service';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css']
})
export class ShoppingListComponent implements OnInit {
 accesserieses: Accesseries[]; 
  constructor(private slService: ShoppingListService) { }

  ngOnInit() {
    this.accesserieses = this.slService.getAccesseries();
    this.slService.accesseriesChange
     .subscribe(
        (accesserieses: Accesseries[])=> {
          this.accesserieses = accesserieses;
        }
     );

  }
  //onAccesseriesAdded(accesseries: Accesseries){
   //this.accesserieses.push(accesseries);
  //}

}
