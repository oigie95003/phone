import { Injectable } from '@angular/core';
import { Accesseries } from '../shared/accesseries.model';
import { EventEmitter } from '@angular/core';


  @Injectable()
  
export class ShoppingListService {
  accesseriesChange = new EventEmitter<Accesseries[]>();
  private accesserieses: Accesseries[] = [
    new Accesseries('Nokia', 5),
    new Accesseries('Samsung', 10),
  ];
  constructor(private elService: ShoppingListService){ }
  getAccesseries() {
    return this.accesserieses.slice();
  }
  addAccesseries(accesseries:Accesseries){
   this.accesserieses.push(accesseries);
   this.accesseriesChange.emit(this.accesserieses.slice());
  

  }
}

