import { Injectable } from '@angular/core';
import { Phone } from './phone. model';
import { EventEmitter, Output } from '@angular/core';


@Injectable()

export class PhoneService {
  @Output() phoneSealed = new EventEmitter<Phone>();
 private phones: Phone [] = [
    new Phone('Office Phone','Very powerful',
    'https://upload.wikimedia.org/wikipedia/en/d/d1/Nortel_1165E_Telephone.jpg'),
    new Phone('Self Phone', 'Meant for student', 
    'https://upload.wikimedia.org/wikipedia/commons/1/10/Japanese_mobile_phone_keyboard.jpg' ),
    new Phone( 'Sony Errison', 'China','https://c1.staticflickr.com/1/107/267108001_bae6a1aa9e_b.jpg'),
    new Phone('Common','ipad and phone',
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSwyPhFmFYb0D0x8hhL8ikjI-afxpocq9e8kc9nggy5B_gdxCx8'),
   new Phone ('Old Nokia','Japanese','https://upload.wikimedia.org/wikipedia/commons/1/1d/Cordless.phone.750pix.jpg')
   ];
  getPhones(){
    return this.phones.slice();
  }
  //this.phoneSealed.emit();
}
