import { Component, OnInit, Input } from '@angular/core';
import { Phone } from './phone. model';
import { PhoneService } from './phone.service';

@Component({
  selector: 'app-phone',
  templateUrl: './phone.component.html',
  styleUrls: ['./phone.component.css'],
  providers: [PhoneService]
})
export class PhoneComponent implements OnInit {
 @Input() sealedPhone: Phone;
  constructor(private phoneService: PhoneService) { }

  ngOnInit() {
    this.phoneService.phoneSealed
     .subscribe(
       (phone: Phone)=>{
         this.sealedPhone = phone;
       }

     );

  }

}
