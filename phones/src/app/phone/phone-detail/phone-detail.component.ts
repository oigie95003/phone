import { Component, OnInit, Input, } from '@angular/core';
import { Phone } from '../phone. model';

@Component({
  selector: 'app-phone-detail',
  templateUrl: './phone-detail.component.html',
  styleUrls: ['./phone-detail.component.css']
})
export class PhoneDetailComponent implements OnInit {
 @Input() phone: Phone;
 public imagePath: string;
 
  constructor(imagePath: string) { 
     this.imagePath = 'imagePath';
}
  ngOnInit() {
  }

}
