import { Component, OnInit,  Output } from '@angular/core';
import { Phone } from '../phone. model';
import { PhoneService } from '../phone.service';


@Component({
  selector: 'app-phone-list',
  templateUrl: './phone-list.component.html',
  styleUrls: ['./phone-list.component.css']
})
export class PhoneListComponent implements OnInit {
 // @Output() phoneWasSealed = new EventEmitter<Phone>();
  phones: Phone [];
  constructor(private phoneService: PhoneService) { }

  ngOnInit() {
    this.phones = this.phoneService.getPhones();
  }
//onPhoneSealed(phone: Phone){
    //this.phoneWasSealed.emit(phone);
}

