import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Phone } from '../../phone. model';
import { PhoneService } from '../../phone.service';

@Component({
  selector: 'app-phone-item',
  templateUrl: './phone-item.component.html',
  styleUrls: ['./phone-item.component.css']
})
export class PhoneItemComponent implements OnInit {
 @Input() phone: Phone;
 
  constructor(private phoneService: PhoneService) { }

  ngOnInit() {
  }
  onSealed(){
   this.phoneService.phoneSealed.emit(this.phone);
  }

}
